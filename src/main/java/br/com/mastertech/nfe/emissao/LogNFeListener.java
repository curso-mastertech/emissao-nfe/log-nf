package br.com.mastertech.nfe.emissao;

import br.com.mastertech.nfe.model.LogNFe;
import br.com.mastertech.nfe.service.LogEmissaoNFeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogNFeListener {

    public static final String TOPICO_LOG_NFE = "pedro-biro-2";

    @Autowired
    private LogEmissaoNFeService logService;

    @KafkaListener(topics = TOPICO_LOG_NFE, groupId = "grupo1")
    public void registraResultado(@Payload LogNFe logNFe) {
        logService.log(logNFe);
    }
}
