package br.com.mastertech.nfe.model;

public class LogNFe {

    private String identidade;
    private String tipo;
    private String valor;
    private String timestamp;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isEmissao() {
        return "emissao".equalsIgnoreCase(this.tipo);
    }

    public boolean isConsulta() {
        return "consulta".equalsIgnoreCase(this.tipo);
    }
}
