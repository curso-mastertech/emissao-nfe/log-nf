package br.com.mastertech.nfe.service;

import br.com.mastertech.nfe.model.LogNFe;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class LogEmissaoNFeService {
    public static final String ARQUIVO_LOGS_NFE = "logs-nfe.txt";

    public void log(LogNFe logNFe) {
        if (logNFe.isEmissao()) logEmissao(logNFe);
        if (logNFe.isConsulta()) logConsulta(logNFe);
    }

    private void logEmissao(LogNFe op) {
        String log = String.format("[%s] [ %s]: %s acaba de pedir a emissao de uma NF no valor de R$ %s!\n", op.getTimestamp(), op.getTipo(), op.getIdentidade(), op.getValor());
        escreverLogArquivo(log);
    }

    private void logConsulta(LogNFe op) {
        String log = String.format("[%s] [%s]: %s acaba de pedir os dados das suas notas fiscais.\n", op.getTimestamp(), op.getTipo(), op.getIdentidade());
        escreverLogArquivo(log);
    }

    private void escreverLogArquivo(String log) {
        try {
            Files.write(Paths.get(ARQUIVO_LOGS_NFE), log.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}